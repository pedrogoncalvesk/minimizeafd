import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

// Defines finite deterministic automata with an 5-tupla:

public class AFD {
		
	private int 	    n;
	private int 	    s;
	private int 	    q0;
	private Integer[]   F;
	private Integer[][] delta;
	
	public AFD clone(){
		AFD clone = new AFD();
		clone.n = this.n;
		clone.s = this.s;
		clone.q0 = this.q0;
		clone.F = new Integer[this.F.length];
		for (int i = 0; i < this.F.length; i++) {
			clone.F[i] = this.F[i];
		}
		clone.delta = new Integer[this.delta.length][];
		for (int i = 0; i < this.delta.length; i++) {
			clone.delta[i] = new Integer[this.delta[i].length];
			for (int j = 0; j < this.delta[i].length; j++) {
				clone.delta[i][j] = this.delta[i][j];
			}
		}
		return clone;
	}
	
	public boolean setIntegerVariables (ArrayList<Integer> line) {
		if (line.size() != 3)
			return false;
		
		this.n  = line.get(0);
		this.s  = line.get(1);
		this.q0 = line.get(2); 
		return true;
	}
	
	private boolean checkAcceptancesStates(){
		boolean result = false;
		for (int i = 0; i < this.F.length; i++) {
			if(this.F[i] == 1)
				result = true;
		}
		return result;
	}
	
	public boolean setAcceptancesStates (List<Integer> line) {
		if (line.size() != this.n)
			return false;
		
		this.F = new Integer[this.n];
		for (int i = 0; i < this.F.length; i++) {
			this.F[i] = line.get(i);
		}
		return checkAcceptancesStates();
	}
	
	public boolean setDelta (HashMap<Integer, ArrayList<Integer>> map) {
		if (map.size() != this.n)
			return false;
		
		this.delta = new Integer[this.n][this.s];
		for (int i = 0; i < this.delta.length; i++) {
			ArrayList<Integer> line = map.get(i);
			if (line.size() != this.s) 
				return false;			
			for (int j = 0; j < this.delta[i].length; j++) {
				this.delta[i][j] = line.get(j);
			}
		}
		return true;
	}
	
	public int getN() {
		return this.n;
	}
	public int getS() {
		return this.s;
	}
	public int getQ0() {
		return this.q0;
	}
	public String getFToString() {
		String result = "";
		for (int i = 0; i < this.F.length; i++) {
			result += this.F[i] + " ";
		}
		return result;
	}
	public Integer[] getF() {
		return this.F;
	}
	public String[] getDeltaToString() {
		String[] result = new String[this.n];
		for (int i = 0; i < this.delta.length; i++) {
			String line = "";
			for (int j = 0; j < this.delta[i].length; j++) {
				line += this.delta[i][j] + " ";
			}
			result[i] = line;
		}
		return result;
	}
	public Integer[][] getDelta() {
		return this.delta;
	}
	
	public void printAFD(){
		System.out.println(this.n+" "+this.s+" "+this.q0);
		for (int i = 0; i < this.F.length; i++) {
			System.out.print(this.F[i]+" ");
		}
		System.out.println();
		for (int i = 0; i < this.delta.length; i++) {
			for (int j = 0; j < this.delta[i].length; j++) {
				System.out.print(this.delta[i][j]+" ");
			}
			System.out.println();
		}
	}
}
