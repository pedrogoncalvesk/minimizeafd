import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;


public class AFDtoAFDMin extends AFD {
	
	@SuppressWarnings("unused")
	private AFD afd;
	private AFD afdmin;
	
	/*
	 ** O HashMap accessStates representa o vetor de estados inacessiveis.
	 ** Padrao do ArrayList: 
	 **		Posicao 0: representa a marcacao de acessibilidade
	 **			0-> inacess�vel
	 **			1-> acess�vel
	 **		Posicao 1: representa a marcacao de finalizacao
	 **			0-> nao finalizado	
	 **			1-> finalizado
	*/
	private HashMap<Integer,ArrayList<Integer>> accessStates;
	
	/*
	 ** O HashMap utilsStates representa o vetor de estados uteis.
	 ** Padrao do ArrayList: 
	 **		Posicao 0: representa a marca��o de utilidade
	 **			0-> inutil
	 **			1-> util
	 **		Posicao 1: representa a marca��o de finalizacao
	 **			0-> nao finalizado	
	 **			1-> finalizado
	*/
	private HashMap<Integer,ArrayList<Integer>> utilsStates;

	public AFDtoAFDMin(AFD afd) {
		this.afdmin = afd.clone();
		this.afd = afd;
		this.accessStates = new HashMap<Integer,ArrayList<Integer>>();
		initAccessStates();
		this.utilsStates = new HashMap<Integer,ArrayList<Integer>>();
		initUtilsStates();
	}

	private void initAccessStates() {
		ArrayList<Integer> values = null;
		for (int i = 0; i < this.afdmin.getN(); i++) {
			values = new ArrayList<Integer>();
			values.add(0);
			values.add(0);
			this.accessStates.put(i, values);
		}
	}
	
	private void initUtilsStates() {
		ArrayList<Integer> values = null;
		for (int i = 0; i < this.afdmin.getN(); i++) {
			values = new ArrayList<Integer>();
			values.add(0);
			values.add(0);
			this.utilsStates.put(i, values);
		}
	}
	
	private int visitedButNotFinishedState(HashMap<Integer,ArrayList<Integer>> map) {
		int result = -1;
		ArrayList<Integer> current = null;
		for (int i = 0; i < map.size(); i++) {
			current = map.get(i);
			if(current.get(0) == 1 && current.get(1) == 0) {
				result = i;
				break;
			}
		}
		return result;
	}
	
	private int notVisitedState(HashMap<Integer,ArrayList<Integer>> map) {
		int result = -1;
		ArrayList<Integer> current = null;
		for (int i = 0; i < map.size(); i++) {
			current = map.get(i);
			if(current.get(0) == 0 && current.get(1) == 0) {
				result = i;
				break;
			}
		}
		return result;
	}
	
	private ArrayList<Integer> notVisitedStates(HashMap<Integer,ArrayList<Integer>> map) {
		ArrayList<Integer> result = new ArrayList<Integer>();
		ArrayList<Integer> current = null;
		for (int i = 0; i < map.size(); i++) {
			current = map.get(i);
			if(current.get(0) == 0 && current.get(1) == 0) {
				result.add(i);
				break;
			}
		}
		return result;
	}
	
	public void removeInaccessStates(){		
		ArrayList<Integer> initialState = this.accessStates.get(this.afdmin.getQ0());
		initialState.set(0, new Integer(1)); // Estado inicial marcado como visitado
		
		int index = this.afdmin.getQ0();
		
		while (index != -1) {			
			ArrayList<Integer> currentState = this.accessStates.get(index);
			Integer[] currentDelta = this.afdmin.getDelta()[index];
			for (int i = 0; i < currentDelta.length; i++) {
				if (currentDelta[i] != -1){
					ArrayList<Integer> oldAccess = this.accessStates.get(i);
					ArrayList<Integer> newAccess = new ArrayList<Integer>();
					newAccess.add(1);
					newAccess.add(oldAccess.get(1));
					this.accessStates.replace(currentDelta[i], newAccess);
				}
			}
			currentState.set(1, new Integer(1));
			this.accessStates.replace(index, currentState);
			
			index = visitedButNotFinishedState(this.accessStates);
		}
		
		int indexRemove = notVisitedState(this.accessStates);
		while (indexRemove != -1){
			
			HashMap<Integer,ArrayList<Integer>> map = new HashMap<Integer,ArrayList<Integer>>();
			ArrayList<Integer> line = new ArrayList<Integer>();
			
			// Diminui o numero de estados
			line.add(this.afdmin.getN()-1);
			line.add(this.afdmin.getS());
			line.add(this.afdmin.getQ0());
			this.afdmin.setIntegerVariables(line);			
			
			// Acerta o vetor de estado de aceitacao
			line = new ArrayList<Integer>(Arrays.asList(this.afdmin.getF()));
			line.remove(indexRemove);
			this.afdmin.setAcceptancesStates(line);
			
			int x = 0;
			// Acerta a funcao delta
			for (int i = 0; i < this.afdmin.getDelta().length; i++) {
				if(i != indexRemove){
					line = new ArrayList<Integer>(Arrays.asList(this.afdmin.getDelta()[i]));
					map.put(x, line);
					x++;
				}				
			}
			this.afdmin.setDelta(map);
			
			map = new HashMap<Integer,ArrayList<Integer>>();
			x = 0;
			// Acerta o vetor de estados acessiveis
			for (int i = 0; i < this.accessStates.size(); i++) {
				if(i != indexRemove){
					map.put(x, this.accessStates.get(i));
					x++;
				}
			}
			this.accessStates = map;
			
			map = new HashMap<Integer,ArrayList<Integer>>();
			x = 0;			
			// Acerta o vetor de estados inuteis
			for (int i = 0; i < this.utilsStates.size(); i++) {
				if(i != indexRemove){
					map.put(x, this.utilsStates.get(i));
					x++;
				}
			}
			this.utilsStates = map;
			
			indexRemove = notVisitedState(this.accessStates);
		}		
	}
	
	public void printAccessStates(){
		for (int i = 0; i < this.accessStates.size(); i++) {
			System.out.print("q"+i+": ");
			for (int j = 0; j < this.accessStates.get(i).size(); j++) {
				System.out.print((this.accessStates.get(i)).get(j) + " ");
			}
			System.out.println();
		}
	}
	
	public void removeInutilsStates(){
		
		int index = -1;
		
		for (int i = 0; i < this.afdmin.getF().length; i++) {
			if (this.afdmin.getF()[i] == 1) {
				ArrayList<Integer> acceptanceState = this.utilsStates.get(i);
				acceptanceState.set(0, new Integer(1)); // Estado de aceitacao marcado como visitado
				index = i;
			}
		}
		
		while (index != -1) {
			
			ArrayList<Integer> currentState = this.utilsStates.get(index);
			ArrayList<Integer> notVisiteds = notVisitedStates(this.utilsStates); 
			for (int j = 0; j < notVisiteds.size(); j++) {
				int qj = notVisiteds.get(j);
				Integer[] currentDelta = this.afdmin.getDelta()[qj];
				for (int i = 0; i < currentDelta.length; i++) {
					if(currentDelta[i] == index){
						ArrayList<Integer> oldUtil = this.utilsStates.get(qj);
						ArrayList<Integer> newUtil = new ArrayList<Integer>();
						newUtil.add(1);
						newUtil.add(oldUtil.get(1));
						this.utilsStates.replace(currentDelta[i], newUtil);
					}
				}				
			}
			currentState.set(1, new Integer(1));
			this.utilsStates.replace(index, currentState);
			
			index = visitedButNotFinishedState(this.utilsStates);
		}
		
		int indexRemove = notVisitedState(this.utilsStates);
		while (indexRemove != -1){
			
			HashMap<Integer,ArrayList<Integer>> map = new HashMap<Integer,ArrayList<Integer>>();
			ArrayList<Integer> line = new ArrayList<Integer>();
			
			// Diminui o numero de estados
			line.add(this.afdmin.getN()-1);
			line.add(this.afdmin.getS());
			line.add(this.afdmin.getQ0());
			this.afdmin.setIntegerVariables(line);			
			
			// Acerta o vetor de estado de aceitacao
			line = new ArrayList<Integer>(Arrays.asList(this.afdmin.getF()));
			line.remove(indexRemove);
			this.afdmin.setAcceptancesStates(line);
			
			int x = 0;
			// Acerta a funcao delta
			for (int i = 0; i < this.afdmin.getDelta().length; i++) {
				if(i != indexRemove){
					line = new ArrayList<Integer>(Arrays.asList(this.afdmin.getDelta()[i]));
					map.put(x, line);
					x++;
				}
			}
			this.afdmin.setDelta(map);
			
			map = new HashMap<Integer,ArrayList<Integer>>();
			x = 0;
			// Acerta o vetor de estados acessiveis
			for (int i = 0; i < this.accessStates.size(); i++) {
				if(i != indexRemove){
					map.put(x, this.accessStates.get(i));
					x++;
				}
			}
			this.accessStates = map;
			
			map = new HashMap<Integer,ArrayList<Integer>>();
			x = 0;			
			// Acerta o vetor de estados inuteis
			for (int i = 0; i < this.utilsStates.size(); i++) {
				if(i != indexRemove){
					map.put(x, this.utilsStates.get(i));
					x++;
				}
			}
			this.utilsStates = map;
			
			indexRemove = notVisitedState(this.utilsStates);
		}
	}
	
	public void printUtilsStates(){
		for (int i = 0; i < this.utilsStates.size(); i++) {
			System.out.print("q"+i+": ");
			for (int j = 0; j < this.utilsStates.get(i).size(); j++) {
				System.out.print((this.utilsStates.get(i)).get(j) + " ");
			}
			System.out.println();
		}
	}
}
