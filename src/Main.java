import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;


public class Main {
	
	private static ArrayList<Integer> readLine(StringTokenizer token){
		if (token.countTokens() == 0)
        	return null;
        
		ArrayList<Integer> line = new ArrayList<Integer>();
        while(token.hasMoreTokens()){
        	line.add(Integer.parseInt(token.nextToken()));
        }        
        return line;
	}
	
	private static HashMap<Integer, ArrayList<Integer>> readLines(BufferedReader fileInput, int numberLines) throws IOException {
		StringTokenizer token = null;
		ArrayList<Integer> line = null;
		HashMap<Integer, ArrayList<Integer>> map = new HashMap<Integer, ArrayList<Integer>>();
		
		for (int i = 0; i < numberLines; i++) {
			token = new StringTokenizer(fileInput.readLine());
			if (token.countTokens() == 0)
	        	return null;
			
			line = new ArrayList<Integer>();
	        while(token.hasMoreTokens()){
	        	line.add(Integer.parseInt(token.nextToken()));
	        }
	        map.put(i, line);
		}
		return map;
	}
	
	private static void setInputFileToAFD(AFD afd, String input) throws IOException {
		File file = new File(input); 
		FileReader fileR = new FileReader(file);		
		BufferedReader reader = new BufferedReader(fileR);
		
		try {
			ArrayList<Integer> firstLine = readLine(new StringTokenizer(reader.readLine()));
			if(!afd.setIntegerVariables(firstLine)) {
				System.err.println("Primeira linha inv�lida.\nPor favor, insira: n s q0");
				return;
			}			
			
			ArrayList<Integer> secondLine = readLine(new StringTokenizer(reader.readLine()));
			if(!afd.setAcceptancesStates(secondLine)){
				System.err.println("Segunda linha inv�lida.\nPor favor, insira "+ afd.getN() +" estados com valores 0 ou 1");
				return;
			}
			
			HashMap<Integer, ArrayList<Integer>> transitionFunction = readLines(reader, afd.getN());
			if(!afd.setDelta(transitionFunction)){
				System.err.println("Fun��o de transi��o inv�lida.");
				return;
			}
			
		} catch (NullPointerException e){
			System.err.println("Formato do arquivo '"+ file.getName() +"' inv�lido.");
			return;
		} finally {
			reader.close();
		}
	}
	
	@SuppressWarnings("finally")
	private static boolean writeLine(String line, String output, boolean append) throws IOException{
		
		File file = new File(output);
		FileWriter fileW = new FileWriter(file, append);
		BufferedWriter writer = new BufferedWriter(fileW);
		
        try {
            writer.write(line);
            writer.newLine();
        } finally {
            try {
                writer.close();
                return true;
            } catch (Exception e) {
            	return false;
            }
        }
	}
	
	@SuppressWarnings("finally")
	private static boolean writeLines(String[] lines, String output, boolean append) throws IOException{
		
		File file = new File(output);
		FileWriter fileW = new FileWriter(file, append);
		BufferedWriter writer = new BufferedWriter(fileW);
		
        try {
        	for (int i = 0; i < lines.length; i++) {
        		writer.write(lines[i]);
        		if((i+1) != lines.length)
        			writer.newLine();
			}            
        } finally {
            try {
                writer.close();
                return true;
            } catch (Exception e) {
            	return false;
            }
        }
	}
	
	private static boolean setAFDToOutputFile(AFD afd, String output) throws IOException {
		
		String firstLine = afd.getN()+" "+afd.getS()+" "+afd.getQ0();
		if(!writeLine(firstLine, output, false))
			return false;
		String secondLine = afd.getFToString();
		if(!writeLine(secondLine, output, true))
			return false;
		String[] transitionFunction = afd.getDeltaToString();
		if(!writeLines(transitionFunction, output, true))
			return false;
		return true;
	}
	
	public static void main(String[] args) throws IOException {
		
		AFD afd = new AFD();
		
		try {
			setInputFileToAFD(afd, args[0]);
			
			AFDtoAFDMin afdmin = new AFDtoAFDMin(afd);
			
			afdmin.printAccessStates();
			System.out.println();
			afdmin.removeInaccessStates();
			afdmin.printAccessStates();
			
			
			System.out.println();
			System.out.println();
			
			
			afdmin.printUtilsStates();
			System.out.println();
			afdmin.removeInutilsStates();
			afdmin.printUtilsStates();
			
//			setAFDToOutputFile(afd, args[1]);
		} catch (ArrayIndexOutOfBoundsException e ) {
			String index = e.getLocalizedMessage();
			System.err.print("Arquivo n�o especificado => args[" + index + "]\n");
			if (index.equals("1")) {
				System.out.print("Criando arquivo output: ");
				String name = (args[0].split("[.]"))[0]+"m.txt";
				System.out.println(name);
				setAFDToOutputFile(afd, name);
			} else {
				return;	
			}
		}
	}
}
